<div class="row p-5">
    <div class="col-sm-12 col-lg-3"></div>
    <div class="col-sm-12 col-lg-6 ">
        <div class="container shadow-lg bg-transparent">
            <div class="card bg-transparent border-0 pt-4">
                <?php 
                    if ($userDetail->profile_picture) {
                        $profile = $userDetail->profile_picture;
                    } else {
                        $profile = 'default-profile.png';
                    }
                ?>
              <?= $this->Html->image(
                            'profiles/'.$profile, 
                            ['alt' => '',
                                'class' => 'rounded-circle align-self-center border border-white',
                                'width' => '125',
                                'height' => '125'
                            ]
                        ) ?>
              <div class="card-body text-center pt-2">
                <span class="font-italic text-muted">@<?= $userDetail->username ?></span>
                <h5 class="card-title font-weight-bold route-user"
                    data-userId="<?= $userDetail->id ?> ">
                    <?= $userDetail->first_name ?>
                    <?= $userDetail->last_name ?>
                </h5>
              </div>
            </div>
        </div>
        <div class="container shadow-lg bg-transparent mt-2 py-2">
            <h5>Followers
                <span class="badge badge-light ml-2">
                    <?= $this->Paginator->params()['count'] ?>
                </span>
            </h5>
            <hr>
            <div class="row">
                <div class="col-sm-12" >
                    <?php if (!$followers->isEmpty()): ?>
                    <?php foreach ($followers as $key => $follower): ?>
                    <div class="card">
                        <div class="card-body py-2">
                            <?php 
                                if($follower->user->profile_picture) {
                                    $profile = $follower->user->profile_picture;
                                } else {
                                    $profile = 'default-profile.png';
                                }
                            ?>
                            <?= $this->Html->image(
                                'profiles/'.$profile, 
                                ['alt' => '',
                                    'class' => 'rounded-circle d-inline-block',
                                    'width' => '50',
                                    'height' => '50'
                                ]
                            ) ?>
                            <div class="d-inline-block align-bottom pl-2">
                                <h6 class="font-weight-bold m-0 route-user"
                                    data-userId="<?= $follower->user->id ?>">
                                    <?= $follower->user->first_name ?> 
                                    <?= $follower->user->last_name ?>
                                </h6>
                                <h6 class="small font-italic text-muted">
                                    @<?= $follower->user->username ?>
                                </h6>
                            </div>
                            <?php 
                                if ($follower->user->id == $authUser['id']) {
                                    $displayClass = 'd-none';
                                } else {
                                    $displayClass = 'd-inline-block';
                                }
                            ?>
                            <div class="<?= $displayClass ?> align-middle float-right line-hieght-50">
                                <?php 
                                    if(isset($follower->user->user_following->id)) {
                                        $followLabel = 'Unfollow';
                                        $class = 'btn-secondary px-2';
                                    } else {
                                        $followLabel = 'Follow';
                                        $class = 'btn-outline-secondary px-3';
                                    }
                                ?>
                                <?= $this->Form->postButton(
                                    $followLabel,
                                    [
                                        'controller' => 'follows',
                                        'action' => 'follow',
                                        $follower->user->id
                                    ],
                                    ['class' => "btn btn-sm {$class}"]
                                ) ?>
                                    
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                    <?= $this->element('common/pagination') ?>
                    <?php else: ?>
                    <div class="card">
                        <div class="card-body text-center  mt-4">
                            <p class="card-text">The user doesn't have followers at the moment.</p>
                        </div>
                    </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-lg-3"></div>
</div>
<?= $this->Html->script('follows') ?>