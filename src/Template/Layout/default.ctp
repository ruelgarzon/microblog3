<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Microblog 3';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('bootstrap.min') ?>
    <?= $this->Html->css('open-iconic/font/css/open-iconic-bootstrap') ?>
    <?= $this->Html->css('custom') ?>

    <?= $this->Html->script('jquery-3.4.1.min') ?>
    <?= $this->Html->script('popper.min') ?>
    <?= $this->Html->script('bootstrap.bundle.min') ?>
    <?= $this->Html->script('jquery.timeago') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="masthead pb-5">
    <input type="hidden" id="webroot" value="<?= $this->request->getAttribute("webroot") ?>">
    <header class="masthead-page <?= ($authUser) ? '' : ' d-none '?>">
        <div class="container">
            <nav class="navbar masthead-page">
                 <?= $this->Html->link(
                            $this->Html->image(
                                'logo.png', 
                                ['alt' => '',
                                    'class' => 'rounded-circle d-inline-block align-top',
                                    'width' => '50',
                                    'height' => '50'
                                ]
                            ).
                            '<h2 class="header-brand d-inline-block align-bottom pl-3 text-white">
                                MicroBlog
                            </h2>',
                            ['controller' => 'posts', 'action' => 'index'],
                            ['class' => 'btn btn-link btn-lg',
                                'escape' => false
                            ]
                        ) ?>
                
                <?= $this->Form->create('Search', 
                    [
                        'url' => ['controller' => 'searches', 'action' => 'index'],
                        'class' => 'form-inline w-50',
                        'style' => 'justify-content: flex-end;'
                    ]
                ) ?>
                    <div class="input-group input-group-sm w-75">
                        <?= $this->Form->control('type', [
                                'options' => ['user' => 'Users', 'post' => 'Posts'],
                                'class' => 'form-control general-search-dropdown',
                                'templates' => [
                                    'inputContainer' => '{{content}}'
                                ],
                                'label' => false
                            ])
                        ?>
                        <?= $this->Form->control('keyword', [
                                'class' => 'form-control general-search',
                                'placeholder' => 'Search',
                                'templates' => [
                                    'inputContainer' => '{{content}}'
                                ],
                                'label' => false
                            ])
                        ?>
                        <div class="input-group-append">
                            <button class="btn btn-outline-light
                                postComment" 
                                type="submit">
                                <span class="oi oi-magnifying-glass"></span>
                            </button>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
                <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle d-inline-block text-white" href="#" 
                    id="navbarDropdown" role="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @<?= h($authUser['username']) ?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <?= $this->Html->link(
                            $this->Html->tag('span', '', 
                                ['class' => 'oi oi-person mr-2']). ' Profile',
                            'javascript:;',
                            ['class' => 'dropdown-item',
                                'escape' => false,
                                'data-toggle' => 'modal',
                                'data-target' => '#profileModal'
                            ]
                        ) ?>
                        <div class="dropdown-divider"></div>
                        <?= $this->Html->link(
                            $this->Html->tag('span', '', 
                                ['class' => 'oi oi-wrench mr-2']). ' Change Password',
                            'javascript:;',
                            ['class' => 'dropdown-item',
                                'escape' => false,
                                'data-toggle' => 'modal',
                                'data-target' => '#changePasswordModal'
                            ]
                        ) ?>
                        <div class="dropdown-divider"></div>
                        <?= $this->Html->link(
                            $this->Html->tag('span', '', ['class' => 'oi oi-account-login mr-2']). ' Log out',
                            ['controller' => 'users', 'action' => 'logout'],
                            ['class' => 'dropdown-item',
                                'escape' => false
                            ]
                        ) ?>
                    </div>
                    <?= $this->Html->link(
                            $this->Html->tag('span', '', ['class' => 'oi oi-home']),
                            ['controller' => 'posts', 'action' => 'index'],
                            ['class' => 'btn btn-link text-white btn-lg d-none',
                                'escape' => false
                            ]
                        ) ?>
                </div>
            </nav>
        </div>
    </header>

    <?= $this->element('common/edit_profile') ?>
    <?= $this->element('common/change_password') ?>
    
    <?= $this->Html->script('common') ?>

    <div id="content" class="container">
        <?= $this->fetch('content') ?>
    </div>
    <div class="modal fade" id="alertModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-black alert m-0">
                    <button type="button" class="float-right close" 
                        data-dismiss="modal">
                        &times;
                    </button>
                    <div id="notifMsg"><?= html_entity_decode($this->Flash->render()) ?></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->Html->script('user.helper') ?>
</body>
</html>
