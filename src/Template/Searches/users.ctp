<div class="row p-5">
    <div class="col-sm-12 col-lg-3"></div>
    <div class="col-sm-12 col-lg-6 ">
        <div class="container shadow-lg bg-transparent mt-2 py-2">
            <h5>Search Results
                <span class="badge badge-light ml-2">
                    <?= $this->Paginator->params()['count'] ?>
                </span>
                <h6 class="d-inline">
                    Search Type :
                    <span class="badge badge-primary font-weight-light ml-2"> 
                        &nbsp;Users&nbsp;
                    </span>
                </h6> 
                <h6 class="d-inline ml-3">
                    Keyword :
                    <span class="badge badge-primary font-weight-light ml-2">
                        &nbsp;<?= h($keyword) ?>&nbsp;
                    </span>
                </h6> 
            </h5>
            <hr>
            <div class="row">
                <div class="col-sm-12" >
                    <?php if ($users): ?>
                    <?php foreach ($users as $key => $user): ?>
                    <div class="card">
                        <div class="card-body py-2">
                            <?php 
                                if($user->profile_picture) {
                                    $profile = $user->profile_picture;
                                } else {
                                    $profile = 'default-profile.png';
                                }
                            ?>
                            <?= $this->Html->image(
                                'profiles/'.$profile, 
                                ['alt' => '',
                                    'class' => 'rounded-circle d-inline-block',
                                    'width' => '50',
                                    'height' => '50'
                                ]
                            ) ?>
                            <div class="d-inline-block align-bottom pl-2">
                                <h6 class="font-weight-bold m-0 route-user"
                                    data-userId="<?= $user->id ?>">
                                    <?= $user->first_name ?> 
                                    <?= $user->last_name ?>
                                </h6>
                                <h6 class="small font-italic text-muted">
                                    @<?= $user->username ?>
                                </h6>
                            </div>
                            <?php 
                                if ($user->id == $authUser['id']) {
                                    $displayClass = 'd-none';
                                } else {
                                    $displayClass = 'd-inline-block';
                                }
                            ?>
                            <div class="<?= $displayClass ?> align-middle float-right line-hieght-50">
                                <?php 
                                    if ($followed[$key]) {
                                        $followLabel = 'Unfollow';
                                        $class = 'btn-secondary px-2';
                                    } else {
                                        $followLabel = 'Follow';
                                        $class = 'btn-outline-secondary px-3';
                                    }
                                ?>
                                <?= $this->Form->postButton(
                                    $followLabel,
                                    [
                                        'controller' => 'follows',
                                        'action' => 'follow',
                                        $user->id
                                    ],
                                    ['class' => "btn btn-sm {$class}"]
                                ) ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                    <?= $this->element('common/pagination') ?>
                    <?php else: ?>
                        <h6 class="text-center">No results found.</h6>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-lg-3"></div>
</div>
<?= $this->Html->script('search') ?>