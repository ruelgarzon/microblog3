<div class="row p-5">
    <div class="col-sm-12 col-lg-2"></div>
    <div class="col-sm-12 col-lg-8 ">
        <div class="container shadow-lg bg-transparent mt-2 py-2">
            <h5>Search Results
                <span class="badge badge-light ml-2">
                    <?= $this->Paginator->params()['count'] ?>
                </span>
                <h6 class="d-inline">
                    Search Type :
                    <span class="badge badge-primary font-weight-light ml-2"> 
                        &nbsp;Posts&nbsp;
                    </span>
                </h6> 
                <h6 class="d-inline ml-3">
                    Keyword :
                    <span class="badge badge-primary font-weight-light ml-2">
                        &nbsp;<?= h($keyword) ?>&nbsp;
                    </span>
                </h6> 
            </h5>
            <hr>
            <div class="row">
                <div class="col-sm-12" >
                    <?php if ($posts): ?>
                    <?php foreach ($posts as $key => $post): ?>
                    <div class="card mb-3">
                        <?= $this->element('posts/post_view',['post' => $post]) ?>
                        <!-- REPOST CONTAINER -->
                        <?php if(isset($post->repost->id)): ?>
                        <?= $this->element('posts/repost_view',['post' => $post]) ?>
                        <?php endif ?>
                        <?= $this->element('posts/comments',[
                            'comment' => $comments[$key],
                            'post' => $post
                        ]) ?>
                    </div>
                    <?php endforeach ?>
                    <?= $this->element('common/pagination') ?>
                    <?php else: ?>
                        <h6 class="text-center">No results found.</h6>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-lg-2"></div>
</div>
<?= $this->element('posts/edit_post') ?>
<?= $this->element('posts/repost') ?>
<?= $this->element('posts/edit_comment') ?>

<?= $this->Html->script('search') ?>
<?= $this->Html->script('post') ?>