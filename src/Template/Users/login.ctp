<div class="users form">
    <div class="row h-100 mt-5">
        <div class="col-sm-12 my-auto">
            <div class="card card-block mx-auto" style="width: 35%;">
                <div class="card">
                    <div class="card-header  align-self-center bg-transparent">
                        <?= $this->Html->image(
                            'logo.png', 
                            ['alt' => 'CakePHP',
                                'class' => 'rounded-circle',
                                'width' => '100',
                                'height' => '100'
                            ]
                        ) ?>
                    <h4>MicroBlog</h4>
                    </div>
                    <div class="card-body">
                        <div id="authenticationAlert" class="alert alert-warning d-none" 
                            role="alert">
                            <?= $this->Flash->render('auth') ?>
                        </div>
                        
                        <?= $this->Form->create(
                                'User', 
                                [
                                    'autocomplete' => 'off',
                                    'novalidate' => true
                                ]
                            ) ?>
                        <div class="form-group">
                            <?= $this->Form->control(
                                    'username',
                                    [
                                        'label' => 'Username',
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter username'
                                    ]
                                ) ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control(
                                    'password',
                                    [
                                        'label' => 'Password',
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter password'
                                    ]
                                ) ?>
                        </div>
                        <?= $this->Form->button(
                                'Log In',
                                [
                                    'class' => 'btn btn-primary btn-block',
                                    'div' => [
                                        'class' => 'text-center px-5 mt-5',
                                    ]
                                ]
                            );
                        ?>
                        <?= $this->Form->end() ?>
                        <div class="text-center font-italic pt-3 small">
                            No Account yet? 
                            <?= $this->Html->link(
                                'Sign up here.',
                                ['controller' => 'users', 'action' => 'add'],
                                ['class' => 'font-italic']
                            ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>
<?= $this->Html->script('login') ?>