<div class="row p-5">
    <div class="col-sm-12 col-lg-5 ">
        <!-- PROFILE -->
        <div class="container shadow-lg bg-transparent">
            <div class="card bg-transparent border-0 pt-4">
                <?php 
                    if ($userDetail->profile_picture) {
                        $myProfile = $userDetail->profile_picture;
                    } else {
                        $myProfile = 'default-profile.png';
                    }
                ?>
              <?= $this->Html->image(
                            'profiles/'.$myProfile, 
                            ['alt' => '',
                                'class' => 'rounded-circle align-self-center border border-white',
                                'width' => '125',
                                'height' => '125'
                            ]
                        ) ?>
              <div class="card-body text-center pt-2">
                <span class="font-italic text-muted">@<?= $userDetail->username ?></span>
                <h5 class="card-title font-weight-bold">
                    <?= $userDetail->first_name ?>
                    <?= $userDetail->last_name ?>
                </h5>
                <?php if($editAccess == false): ?>
                    <?php 
                        if($followedThisUser) {
                            $followLabel = 'Unfollow';
                            $followClass = 'btn-secondary';
                        } else {
                            $followLabel = 'Follow';
                            $followClass = 'btn-outline-secondary';
                        }
                    ?>
                <?= $this->Form->postButton(
                                $followLabel,
                                ['controller' => 'follows', 
                                    'action' => 'follow',
                                    $userDetail->id
                                ],
                                ['class' => "btn {$followClass} btn-sm px-5"]
                            ) ?>
                <?php endif ?>
              </div>
            </div>
        </div>
        <!-- FOLLOWING / FOLLOWERS -->
        <div class="container shadow-lg bg-transparent mt-2 py-2">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link text-dark border active" data-toggle="tab" href="#following">
                        Following
                        <span class="badge badge-secondary ml-2"><?= $followingCount ?></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-dark border" data-toggle="tab" href="#follower">
                        Followers
                        <span class="badge badge-secondary ml-2"><?= $followerCount ?></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane container active p-0" id="following">
                    <?php $followingParams = [
                        'followings' => $followings,
                        'followingCount' => $followingCount,
                        'editAccess' => $editAccess
                    ] ?>
                    <?= $this->element('posts/following_view',$followingParams) ?>
                </div>
                <div class="tab-pane container fade p-0" id="follower">
                    <?php $followerParams = [
                        'followers' => $followers,
                        'followerCount' => $followerCount,
                        'editAccess' => $editAccess
                    ] ?>
                    <?= $this->element('posts/follower_view',$followerParams) ?>
                </div>
            </div>
        </div>
    </div>
    <!-- CREATE POST -->
    <div class="col-sm-12 col-lg-7">
        <div class="container shadow-lg bg-transparent p-3 mb-2 <?= $hideAccess ?>">
            <?= $this->Form->create($post, [
                'enctype'=>'multipart/form-data',
                'novalidate' => true,
                'url' => array('controller' => 'posts', 'action' => 'add')
            ]) ?>
                <div class="form-group">
                    <?=  $this->Form->control('content', [
                        'label' => 'Create Post',
                        'rows' => '4',
                        'class' => 'form-control'
                    ]) ?>
                </div>
                <div class="custom-file w-50">
                   <?=  $this->Form->file('image_name', [
                        'class' => 'custom-file-input',
                        'id' => 'post-attachment'
                    ]) ?>
                    <label class="custom-file-label text-truncate" for="post-attachment">
                        Choose image
                    </label>
                    <?php
                    if ($this->Form->isFieldError('image_name')) {
                        echo $this->Form->error('image_name');
                    }
                    ?>
                </div>
                <div class="float-right">
                    <?= $this->Form->button( 'Post', [
                        'class' => 'btn btn-outline-primary'
                    ]) ?>
                </div>
            <?= $this->Form->end() ?>
        </div>
        <!-- RECENT POST BOARD -->
        <div class="container shadow-lg bg-transparent p-3">
            <h5>Recent Posts</h5>
            <hr>
            <?php if ($posts): ?>
            <?php foreach ($posts as $key => $post): ?>
            <div class="card mb-3">
                <?= $this->element('posts/post_view',['post' => $post]) ?>
                <!-- REPOST CONTAINER -->
                <?php if(isset($post->repost->id)): ?>
                <?= $this->element('posts/repost_view',['post' => $post]) ?>
                <?php endif ?>
                <?= $this->element('posts/comments',[
                    'comment' => $comments[$key],
                    'post' => $post
                ]) ?>
            </div>
            <?php endforeach ?>
            <?= $this->element('common/pagination') ?>
            <?php else: ?>
                <div class="card">
                    <div class="card-body text-center">
                        No available post yet.
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>

<!-- MODALS -->
<?= $this->element('posts/edit_post') ?>
<?= $this->element('posts/repost') ?>
<?= $this->element('posts/edit_comment') ?>
<?= $this->Html->script('post') ?>