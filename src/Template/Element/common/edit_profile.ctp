<div class="modal" id="profileModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <?= $this->Form->create('User', 
                [
                    'enctype'=> 'multipart/form-data',
                    'id' => 'ProfileEditForm',
                    'novalidate' => true,
                    'url' => ['controller' => 'users', 'action' => 'edit']
                ]
            ) ?>
            <?= $this->Form->hidden('id', ['default'=> $authUser['id']]) ?>
            <div class="modal-header">
                <h4 class="modal-title">Edit Profile</h4>
                <button type="button" class="close" data-dismiss="modal">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="w-25 container-fluid align-self-center p-0 py-4 rounded bg-secondary">
                        <?php 
                            if ($authUser['profile_picture']) {
                                $profilePic = $authUser['profile_picture'];
                            } else {
                                $profilePic = 'default-profile.png';
                            }
                        ?>
                        <?= $this->Html->image(
                            'profiles/'.$profilePic, [
                                'alt' => '',
                                'class' => 'rounded-circle border border-white',
                                'width' => '100',
                                'height' => '100',
                                'id' => 'profile-picture-preview'
                            ]
                        ) ?>
                    </div>
                </div>
                <div class="text-center pb-5 mt-2">
                    <div class="custom-file w-25">
                       <?=  $this->Form->file(
                            'profile_picture', 
                            [
                                'class' => 'custom-file-input',
                                'id' => 'profile-picture',
                                'onchange' => 'Common.profilePreview(this)'
                            ]
                        ) ?>
                        <label class="custom-file-label text-truncate text-left" for="profile-picture">Update</label>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6 col-sm-12">
                        <?= $this->Form->control(
                            'first_name',
                            [ 
                                'label' => 'First Name',
                                'class' => 'form-control',
                                'placeholder' => 'Enter first name',
                                'default'=> $authUser['first_name']
                            ]
                        ) ?>
                    </div>
                    <div class="form-group col-md-6 col-sm-12 ">
                        <?= $this->Form->control(
                            'last_name',
                            [
                                'label' => 'Last Name',
                                'class' => 'form-control',
                                'placeholder' => 'Enter last name',
                                'default'=> $authUser['last_name']
                            ]
                        ) ?>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6 col-sm-12">
                        <?= $this->Form->control(
                            'username',
                            [
                                'label' => 'Username',
                                'class' => 'form-control',
                                'placeholder' => 'Enter username',
                                'default'=> $authUser['username']
                            ]
                        ) ?>
                    </div>
                    <div class="form-group col-md-6 col-sm-12 ">
                        <?= $this->Form->control(
                            'email',
                            [
                                'label' => 'Email',
                                'class' => 'form-control',
                                'placeholder' => 'Enter email',
                                'default'=> $authUser['email']
                            ]
                        ) ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?= $this->Form->button( 'Save', [
                    'class' => 'btn btn-outline-primary'
                ]) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>