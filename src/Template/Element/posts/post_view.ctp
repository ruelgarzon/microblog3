<div class="card-body py-2">
    <?php 
        if (isset($post->user->profile_picture)) {
            $profile = $post->user->profile_picture;
        } else {
            $profile = 'default-profile.png';
        }
    ?>
    <?=  $this->Html->image(
            'profiles/'.$profile, 
            [
                'alt' => '',
                'class' => 'rounded-circle d-inline-block',
                'width' => '50',
                'height' => '50'
            ]
        ) ?>
    <div class="d-inline-block align-bottom pl-2">
        <h6 class="font-weight-bold m-0 route-user"
            data-userId="<?= $post->user->id ?>">
            <?= h($post->user->first_name) ?>
            <?= h($post->user->last_name) ?>
            <span class="small font-italic text-muted" >
                @<?= h($post->user->username) ?>
            </span>
        </h6>
        <h6 class="small font-italic text-muted">
            <time class="timeago" datetime="<?= $post->created ?>">
            </time>
        </h6>
    </div>
    <div class="d-inline-block float-right">
        <?php 
            $displayNone = '';
            if ($post->user->id != $authUser['id']) {
                $displayNone = ' d-none ';
            }
        ?>
        <div class="dropdown <?= $displayNone ?>">
            <a class="btn btn-link text-dark" href="#" id="dropdownMenuButton" 
            data-toggle="dropdown" 
            aria-haspopup="true" aria-expanded="false">
                <span class="oi oi-ellipses small"></span>
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <?php
                    $id = $post->id;
                    $content = $post->content;
                    echo $this->Html->link(
                        $this->Html->tag('span', '', ['class' => 'oi oi-pencil']).' Edit',
                        'javascript:;',
                        [
                            'onclick' => "Post.edit(`{$id}`,`{$content}`)",
                            'class' => 'dropdown-item line-hieght-10',
                            'escape' => false
                        ]
                    );
                ?>
                <div class="dropdown-divider"></div>
                <?= $this->Form->postButton(
                        $this->Html->tag('span', '', ['class' => 'oi oi-trash']). ' Delete',
                        [
                            'controller' => 'posts',
                            'action' => 'delete', 
                            $post->id
                        ],
                        [
                            'confirm' => 'Are you sure you want to delete this post?',
                            'class' => 'dropdown-item line-hieght-10',
                            'escape' => false
                        ]
                    ) ?>
            </div>
        </div>
            
    </div>
</div>
<div class="card mb-4 mr-4 ml-5 pl-4 border-0">
    <div class="card-body p-0">
        <p class="card-text"><?= h($post->content) ?></p>
    </div>
    <?php 
    if (isset($post->image_name)) {
        echo  $this->Html->image(
            'posts/'. $post->image_name, 
            ['alt' => '',
                'class' => 'img-fluid img-thumbnail',
                    'style' => ['max-width : 300px']

            ]
        );
    } ?>
</div>