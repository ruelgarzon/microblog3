<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
* Post Likes Controller
*
* @property \App\Model\Table\PostLikesTable $PostLikes
*
*/
class PostLikesController extends AppController 
{

    public function initialize()
    {
        parent::initialize();
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Security->setConfig('unlockedActions', ['like']);
    }
    
    /**
    * Like method
    *
    * This method tags certain posts to liked when triggered by the user.
    *
    * @return json
    */
    public function like() 
    {
        $status = 'error';
        $data = $this->getCleanData();

        $id = $data['post_id'];

        $userId = $this->Auth->User('id');

        $result = $this->PostLikes->findByPost_idAndUser_id($id, $userId)
            ->first();
        
        if($result) {
            if ($this->PostLikes->delete($result)) {
                $status = 'success';
            }
        } else {
            $like = $this->PostLikes->newEntity();
            $data = [
                'user_id' => $userId,
                'post_id' => $id
            ];
            $like = $this->PostLikes->patchEntity($like, $data);

            if ($this->PostLikes->save($like)) {
                $status = 'success';
            }
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode([
              'status' => $status
            ]));
    }
}