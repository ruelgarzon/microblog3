<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
* Searches Controller
*
*/
class SearchesController extends AppController 
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }
    
    /**
    * Index method
    *
    * This method display the search results based on selected type and keyword.
    * This can either displays users or posts.
    *
    * @return \Cake\Http\Response|null
    */
    public function index() 
    {
        $type = '';
        $keyword = '';
        $data = $this->getCleanData();

        if (isset($data['type'])) {
            $type = $data['type'];
            $this->getRequest()->getSession()->delete('SearchKeyword');
        } else {
            if($this->getRequest()->getSession()->check('SearchType')) {
                $type = $this->getRequest()->getSession()->read('SearchType');
            } else {
                $type = 'user';
            }
        }
        if (isset($data['keyword'])) {
            $keyword = $data['keyword'];
        } else {
            if($this->getRequest()->getSession()->check('SearchKeyword')) {
                $keyword = $this->getRequest()->getSession()->read('SearchKeyword');
            }
        }

        $this->getRequest()->getSession()->write('SearchType', $type);
        $this->getRequest()->getSession()->write('SearchKeyword', $keyword);

        $this->set(compact('keyword'));

        if ($type == 'post') {
            $renderForm = 'posts';
            $posts = $this->getPostList($keyword);
            $comments = $this->getPostComments($posts);

            $this->set(compact('comments'));
            $this->set(compact('posts'));
        } else {
            $renderForm = 'users';
            $users = $this->getUsersList($keyword);
            $followed = $this->checkUsersFollowing($users);

            $this->set(compact('followed'));
            $this->set(compact('users'));
        }
        
        $this->render($renderForm);
    }

    /**
    * Get Users List method
    *
    * This method returns the list of active users depending on the provided keyword filter.
    *
    * @param string $keyword Name of the users
    *
    * @return object
    */
    private function getUsersList(string $keyword) : object
    {
        $user = TableRegistry::getTableLocator()->get('Users');
        $this->paginate = [
            'conditions' => [
                'activated' => 1,
                'OR' => [
                    ["CONCAT(first_name,' ',last_name) LIKE" => "%{$keyword}%"],
                    ['username LIKE' => "%{$keyword}%"]
                ]
            ],
            'limit' => 10,
            'order' => ['Posts.created' => 'desc']  
        ];
        $users = $this->paginate($user);

        return $users;
    }

    /**
    * Check User Following method
    *
    * This method checks if the specifid user has been followed by the currently logged in user.
    *
    * @param object $users
    *
    * @return array
    */
    private function checkUsersFollowing(object $users) : array
    {
        $followed = [];
        $sessUid = $this->Auth->User('id');
        
        foreach ($users as $key => $user) {
            $followed[$key] = $this->checkFollowing($user->id, $sessUid);
        }

        return $followed;
    }

    /**
    * Check Following method
    *
    * This method checks if the viewed user has been followed by the currently logged in user.
    *
    * @param int $userId User id
    * @param int $sessUid User id of the currently logged in account
    *
    * @return bool
    */
    private function checkFollowing(int $userId, int $sessUid) : bool
    {

        $follows = TableRegistry::getTableLocator()->get('Follows');
        $query = $follows->findAllByFollowed_idAndFollower_id($userId, $sessUid);
        $result = $query->toArray();

        $return = ($result) ? true : false;

        return $return;
    }

    /**
    * Get Posts List method
    *
    * This method returns the list of active posts depending on the provided keyword filter.
    *
    * @param string $keyword Contents of the post
    *
    * @return object
    */
    private function getPostList(string $keyword) : object
    {
        $post = TableRegistry::getTableLocator()->get('Posts');
        $this->paginate = [
            'contain' => ['Users', 'Reposts.Users'],
            'conditions' => [
                'Posts.content LIKE' => "%{$keyword}%",
                'Posts.deleted' => 0,
            ],
            'limit' => 10,
            'order' => ['Posts.created' => 'desc']  
        ];
        $posts = $this->paginate($post);

        return $posts;
    }

    /**
    * Get PostComments method
    *
    * This method fetches and builds each post's comments.
    *
    * @param object $posts User's posts dataset
    *
    * @return array
    */
    private function getPostComments(object $posts) : array
    {
        $comments = [];
        $sessUid = $this->Auth->User('id');
        foreach ($posts as $key => $post) {
            $postId = $post['id'];

            $PostComments = TableRegistry::getTableLocator()->get('PostComments');
            $comments[$key]['comments'] = $PostComments->find()
                ->where([
                    'PostComments.post_id' => $postId,
                    'PostComments.deleted' => 0
                ])
                ->contain('Users')
                ->toArray();
            $PostComments = TableRegistry::getTableLocator()->get('PostComments');
            $comments[$key]['commentCount'] = $PostComments->find()
                ->where([
                    'post_id' => $postId,
                    'deleted' => 0
                ])
                ->count();

            $PostLikes = TableRegistry::getTableLocator()->get('PostLikes');
            $comments[$key]['likeCount'] = $PostLikes->find()
                ->where(['post_id' => $postId])
                ->count();

            $PostLikes = TableRegistry::getTableLocator()->get('PostLikes');
            $result = $PostLikes->findByPost_idAndUser_id($postId, $sessUid)
                ->first();
            $comments[$key]['userLiked'] = ($result) ? true : false;

        }

        return $comments;
    }

}