<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
* Follows Controller
*
* @property \App\Model\Table\FollowsTable $Follows
*
*/
class FollowsController extends AppController 
{

    public function initialize()
    {
        parent::initialize();
    }
    
    public function follow(int $id) 
    {
        $userId = $this->Auth->User('id');

        $users = TableRegistry::getTableLocator()->get('Users');
        $followedUser = $users->get($id);

        $result = $this->Follows->findByFollowed_idAndFollower_id($id, $userId)
            ->first();

        $users = TableRegistry::getTableLocator()->get('Users');
        $followedUser = $users->get($id);
        $followedName = $followedUser->first_name.' '.$followedUser->last_name;

        if ($result) {
            if ($this->Follows->delete($result)) {
                $this->Flash->success(
                    __('You have unfollowed '. h($followedName))
                );

                return $this->redirect($this->referer());
            }
        } else {
            $follow = $this->Follows->newEntity();
            $data = [
                'follower_id' => $userId,
                'followed_id' => $id
            ];
            $follow = $this->Follows->patchEntity($follow, $data);

            if ($this->Follows->save($follow)) {
                $this->Flash->success(__('You have followed '. h($followedName)));

                return $this->redirect($this->referer());
            }
        }
    }

    public function followers(int $userId = null)
    {
        $userId = ($userId) ? $userId : $this->Auth->User('id');
        $users = TableRegistry::getTableLocator()->get('Users');
        $userDetail = $users->get($userId);

        if (!$userDetail) {
            $this->Flash->error(__('The requested user account is not available.'));
            
            return $this->redirect(['action' => 'index']);
        }

        $sessUid = $this->Auth->User('id');

        $userIdArray[] = $userId;

        $followers = TableRegistry::getTableLocator()->get('Followers');

        $this->paginate = [
            'contain' => ['Users.UserFollowing'],
            'conditions' => [
                'Followers.followed_id' => $userId
            ],
            'limit' => 10 
        ];
        $followers = $this->paginate($followers);
        
        $this->set(compact('userDetail'));
        $this->set(compact('followers'));
    }

    public function following(int $userId = null)
    {
        $userId = ($userId) ? $userId : $this->Auth->User('id');
        $users = TableRegistry::getTableLocator()->get('Users');
        $userDetail = $users->get($userId);

        if (!$userDetail) {
            $this->Flash->error(__('The requested user account is not available.'));

                return $this->redirect(['action' => 'index']);
        }

        $sessUid = $this->Auth->User('id');

        $userIdArray[] = $userId;

        $followings = TableRegistry::getTableLocator()->get('Followings');

        $this->paginate = [
            'contain' => ['Users.UserFollowing'],
            'conditions' => [
                'Followings.follower_id' => $userId
            ],
            'limit' => 10 
        ];
        $followings = $this->paginate($followings);
        
        $this->set(compact('userDetail'));
        $this->set(compact('followings'));
    }
}