<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
* Post Comments Controller
*
* @property \App\Model\Table\PostCommentsTable $PostLikes
*
*/
class PostCommentsController extends AppController 
{

    public function initialize()
    {
        parent::initialize();
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Security->setConfig('unlockedActions', ['writeComment','deleteComment','editComment']);
    }
    
    /**
    * Write Comment method
    *
    * This method saves user inputed comments.
    *
    * @return json
    */
    public function writeComment() 
    {
        $status = 'error';
        $commentParam = '';
        $message = '';
        $id = 0;

        $data = $this->getCleanData();

        $id = $data['post_id'];

        $data['user_id'] = $this->Auth->User('id');

        
        $comment = $this->PostComments->newEntity();
        $comment = $this->PostComments->patchEntity($comment, $data);
        $saveComment = $this->PostComments->save($comment);
        if ($saveComment) {
            $status = 'success';
            $id = $saveComment->id;
            $commentParam = h($data['comment']);
        } else {
            $message = $this->renderErrors($comment->getErrors());
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode([
                'status' => $status,
                'id' => $id,
                'comment' => $commentParam,
                'message' => $message
            ]));
    }

    /**
    * Edit Comment method
    *
    * This method saves user inputed revision on comments.
    *
    * @return json
    */
    public function editComment() 
    {
        $status = 'error';
        $commentParam = '';

        $data = $this->getCleanData();

        $comment = $this->PostComments->get($data['id']);

        if ($this->request->is(['post'])) {
            $comment = $this->PostComments->patchEntity($comment, $data);
            
            if ($this->PostComments->save($comment)) {
                $status = 'success';
                $commentParam = h($data['comment']);
            } else {
                $errorMsgs = $this->renderErrors($comment->getErrors());
            }
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode([
                'status' => $status,
                'comment' => $commentParam
            ]));
    }

    /**
    * Delete Comment method
    *
    * This method removes user comments.
    *
    * @return json
    */
    public function deleteComment() 
    {
        $status = 'error';

        $data = $this->getCleanData();

        $comment = $this->PostComments->get($data['id']);

        if ($this->request->is(['post'])) {
            $comment->deleted = 1;
            if ($this->PostComments->save($comment)) {
                $status = 'success';
            } else {
                $errorMsgs = $this->renderErrors($comment->getErrors());
            }
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode([
                'status' => $status
            ]));
    }
}