<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
* Posts Controller
*
* @property \App\Model\Table\PostsTable $Posts
*
*/
class PostsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Security->setConfig('unlockedActions', ['edit','repost']);
    }

    /**
    * Index method
    *
    * This method display the home page of the user after login. This also displays
    *  posts of the logged in user and the posts of the users he/she follows.
    *
    * @param int|null $userId User  id
    *
    * @return \Cake\Http\Response|null
    */
    public function index(int $userId = null)
    {
        $userId = ($userId) ? $userId : $this->Auth->User('id');
        $users = TableRegistry::getTableLocator()->get('Users');
        $userDetail = $users->find('all')
                        ->where([
                            'activated' => 1,
                            'id' => $userId
                        ])
                        ->first();

        if (!$userDetail) {
            $this->Flash->error(__('The requested user account is not available.'));

                return $this->redirect(['action' => 'index']);
        }

        $sessUid = $this->Auth->User('id');
        $editAccess = ($userId == $this->Auth->User('id')) ? true : false;
        $hideAccess = ($editAccess) ? '' : ' d-none ';

        $followedThisUser = $this->checkFollowing($userId, $sessUid);
        $followers = $this->getFollower($userId, $sessUid);
        $followings = $this->getFollowing($userId, $sessUid);
        $followerCount = $this->getFollowerCount($userId);
        $followingCount = $this->getFollowingCount($userId);

        $this->set(compact('followers', 'followings', 'followerCount', 'followingCount'));
        $this->set(compact('followedThisUser', 'userDetail', 'hideAccess', 'editAccess'));

        if ($editAccess) {
            $userIdArray = $this->getFollowedIds($userId);
        }

        $userIdArray[] = $userId;
        
        $this->paginate = [
            'contain' => ['Users', 'Reposts.Users'],
            'conditions' => [
                'Posts.user_id IN' => $userIdArray,
                'Posts.deleted' => 0,
            ],
            'limit' => 10,
            'order' => ['Posts.created' => 'desc']  
        ];
        $posts = $this->paginate();
        $comments = $this->getPostComments($posts);

        if($this->getRequest()->getSession()->check('PostError')) {
            $post = $this->getRequest()->getSession()->read('PostError');
            $this->getRequest()->getSession()->delete('PostError');
        } else {
            $post = $this->Posts->newEntity();
        }
        $this->set(compact('comments', 'posts', 'post'));

        $this->render('index');
    }

    /**
    * Get PostComments method
    *
    * This method fetches and builds each post's comments.
    *
    * @param object $posts User's posts dataset
    *
    * @return array
    */
    private function getPostComments(object $posts) : array
    {
        $comments = [];
        $sessUid = $this->Auth->User('id');
        foreach ($posts as $key => $post) {
            $postId = $post['id'];

            $PostComments = TableRegistry::getTableLocator()->get('PostComments');
            $comments[$key]['comments'] = $PostComments->find()
                ->where([
                    'PostComments.post_id' => $postId,
                    'PostComments.deleted' => 0
                ])
                ->contain('Users')
                ->toArray();
            $PostComments = TableRegistry::getTableLocator()->get('PostComments');
            $comments[$key]['commentCount'] = $PostComments->find()
                ->where([
                    'post_id' => $postId,
                    'deleted' => 0
                ])
                ->count();

            $PostLikes = TableRegistry::getTableLocator()->get('PostLikes');
            $comments[$key]['likeCount'] = $PostLikes->find()
                ->where(['post_id' => $postId])
                ->count();

            $PostLikes = TableRegistry::getTableLocator()->get('PostLikes');
            $result = $PostLikes->findByPost_idAndUser_id($postId, $sessUid)
                ->first();
            $comments[$key]['userLiked'] = ($result) ? true : false;

        }

        return $comments;
    }

    /**
    * Get Followed Id's method
    *
    * This method fetches all id's of the user followed accounts.
    *
    * @param in $userId User id
    *
    * @return array
    */
    private function getFollowedIds(int $userId) : array
    {
        $follows = TableRegistry::getTableLocator()->get('Follows');
        $query = $follows->find('list', [
                'keyField' => 'id',
                'valueField' => 'followed_id'
            ])
            ->where(['follower_id' => $userId])
            ->toArray();

        return $query;
    }

    /**
    * Check Following method
    *
    * This method checks if the viewed user has been followed by the currently logged in user.
    *
    * @param int $userId User id
    * @param int $sessUid User id of the currently logged in account
    *
    * @return bool
    */
    private function checkFollowing(int $userId, int $sessUid) : bool
    {

        $follows = TableRegistry::getTableLocator()->get('Follows');
        $query = $follows->findAllByFollowed_idAndFollower_id($userId, $sessUid);
        $result = $query->toArray();

        $return = ($result) ? true : false;

        return $return;
    }

    /**
    * Get Follower method
    *
    * This method returns the user's list of followers.
    *
    * @param int $userId User id
    * @param int $sessUid User id of the currently logged in account
    *
    * @return array
    */
    private function getFollower(int $userId, int $sessUid) : array
    {
        $followers = TableRegistry::getTableLocator()->get('Followers');
        $query = $followers->find()
            ->distinct(['Followers.id','UserFollowing.id'])
            ->where(['Followers.followed_id' => $userId])
            ->contain('Users.UserFollowing')
            ->limit(10)
            ->toArray();

        return $query;
    }

    /**
    * Get Following method
    *
    * This method returns the user's list of followed user accounts.
    *
    * @param int $userId User id
    * @param int $sessUid User id of the currently logged in account
    *
    * @return array
    */
    private function getFollowing(int $userId, int $sessUid) : array
    {
        $followings = TableRegistry::getTableLocator()->get('Followings');
        $query = $followings->find()
            ->distinct(['Followings.id', 'UserFollowing.id'])
            ->where(['Followings.follower_id' => $userId])
            ->contain('Users.UserFollowing')
            ->limit(10)
            ->toArray();

        return $query;
    }

    /**
    * Get Follower Count method
    *
    * This method returns the user's total number of followers.
    *
    * @param int $userId User id
    *
    * @return int
    */
    private function getFollowerCount(int $userId) : int
    {
        $followers = TableRegistry::getTableLocator()->get('Followers');
        $query = $followers->find()
            ->where(['followed_id' => $userId])
            ->count();

        return $query;
    }

    /**
    * Get Following Count method
    *
    * This method returns the user's total number of followed user accounts.
    *
    * @param int $userId User id
    *
    * @return int
    */
    private function getFollowingCount(int $userId) : int
    {
        $followings = TableRegistry::getTableLocator()->get('Followings');
        $query = $followings->find()
            ->where(['follower_id' => $userId])
            ->count();

        return $query;
    }

    /**
    * Add method
    *
    * @return Redirects on successful add, renders view otherwise.
    */
    public function add()
    {
        $data = $this->getCleanData();
        
        if ($this->request->is('post')) {

            if ($data['image_name']['name'] != '') {

                $extn = substr($data['image_name']['name'], 
                    strrpos($data['image_name']['name'], '.')+1);
                $image = "post_" . $this->Auth->user('id'). "_" . date('His') .  "." . $extn;
                $img = WWW_ROOT . 'img/posts/' . $image;
                
                move_uploaded_file($data['image_name']['tmp_name'], $img);

                $data['image_name'] = $image;
            } else {
                unset($data['image_name']);
            }

            $data['user_id'] = $this->Auth->user('id');

            $post = $this->Posts->newEntity();
            $post = $this->Posts->patchEntity($post, $data);
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('Your post has been saved.'));

                return $this->redirect($this->referer());
            }
            // $errorMsgs = $this->renderErrors($post->getErrors());
            $this->Flash->error('Error in saving your post. Please check details in the form.');
        }
        $this->getRequest()->getSession()->write('PostError', $post);

        return $this->redirect($this->referer());
    }

    /**
    * Edit method
    *
    * @param int $data['id'] Post id.
    * @return Redirects on successful edit, renders error view otherwise.
    * @throws NotFoundException When record not found.
    */
    public function edit()
    {
        $data = $this->getCleanData();

        $post = $this->Posts->get($data['id']);

        if (!$post) {
            throw new NotFoundException(__('Invalid action.'));
        }
        if ($data['remove_image']) {
            $this->unlinkPostImage($post->image_name);
            $data['image_name'] = null;
        } else {
            if ($data['image_name']['name'] != '' ) {

                $extn = substr($data['image_name']['name'], 
                    strrpos($data['image_name']['name'], '.')+1);

                $image = "user_" . $this->Auth->user('id'). "_" . date('His') .  "." . $extn;

                if($post->image_name) {
                    $oldImg = WWW_ROOT . 'img/posts/' . $post->image_name;
                    if(file_exists($oldImg)) {
                        unlink($oldImg);
                    }
                }

                $img = WWW_ROOT . 'img/posts/' . $image;
                
                move_uploaded_file($data['image_name']['tmp_name'], $img);

                $data['image_name'] = $image;
            } else {
                unset($data['image_name']);
            }
        }
        
        if ($this->request->is(['post'])) {
            $post = $this->Posts->patchEntity($post, $data);
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('Your post has been updated'));

                return $this->redirect($this->referer());
            }
            $errorMsgs = $this->renderErrors($post->getErrors());
            $this->Flash->error($errorMsgs);

            return $this->redirect($this->referer());
        }
        $this->set(compact('post'));
    }

    /**
    * Edit method
    *
    * This method removes/delete previously uploaded file from the server.
    *
    * @param string $filename Post attachment filename.
    * @return void
    */
    private function unlinkPostImage(string $filename = null)
    {
        // die($filename);
        if ($filename) {
            $oldImg = WWW_ROOT . 'img/posts/' . $filename;
            if (file_exists($oldImg)) {
                unlink($oldImg);
            }
        }
    }

    /**
    * Delete method
    *
    * @param int $id Post id.
    * @return Redirects on successful update, renders error view otherwise.
    * @throws NotFoundException When record not found.
    */
    public function delete(int $id) 
    {
        $userId = $this->Auth->User('id');

        $post = $this->Posts->get($id);

        if (!$post OR $post->user_id != $userId) {
            throw new NotFoundException(__('Invalid action.'));
        }
        $post->deleted = 1;
        if ($this->Posts->save($post)) {
            $this->Flash->success(
                __('Post has been successfully deleted')
            );
        }

        return $this->redirect($this->referer());
    }

    /**
    * Repost method
    *
    * @return Redirects on successful save, renders view otherwise.
    */
    public function repost()
    {
        $data = $this->getCleanData();
        
        if ($this->request->is('post')) {

            $data['user_id'] = $this->Auth->user('id');

            $post = $this->Posts->newEntity();
            $post = $this->Posts->patchEntity($post, $data);
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('Your post has been saved.'));

                return $this->redirect($this->referer());
            }
            $errorMsgs = $this->renderErrors($post->getErrors());
            $this->Flash->error($errorMsgs);

            return $this->redirect($this->referer());
        }
        $this->set(compact('post'));
    }

}
