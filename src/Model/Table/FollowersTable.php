<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class FollowersTable extends Table
{
   
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('follows');
        $this->setPrimaryKey('follower_id');

        $this->belongsTo('Users', [
            'foreignKey' => 'follower_id'
        ]);
    }
}
