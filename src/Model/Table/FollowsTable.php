<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class FollowsTable extends Table
{
   
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('follows');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Follower', [
            'className' => 'Users',
            'foreignKey' => 'follower_id'
        ]);

        $this->belongsTo('Following', [
            'className' => 'Users',
            'foreignKey' => 'followed_id'
        ]);

        $this->hasOne('FollowUser', [
            'className' => 'Users',
            'foreignKey' => 'followed_id.follower_id'
        ]);
    }
}
