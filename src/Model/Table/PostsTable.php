<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class PostsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('posts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users')
            ->setForeignKey('user_id');

        $this->hasMany('PostLikes')
            ->setForeignKey('post_id');

        $this->hasMany('PostComments')
            ->setForeignKey('post_id');

        $this->hasMany('Reposts')
            ->setForeignKey('parent_id');

        $this->belongsTo('Reposts', ['className' => 'Posts'])
            ->setForeignKey('parent_id');
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('user_id')
            ->notEmptyString('user_id', 'Invalid action. Cannot access session user id.');

        $validator
            ->requirePresence('content')
            ->maxLength('content', 140)
            ->notEmptyString('content', 'Post content is required');


        $validator
            ->allowEmptyFile('image_name')
            ->add('image_name', 'validExtension', [
                'rule' => ['extension', ['png', 'jpg', 'gif', 'jpeg']], 
                'message' => 'Please supply a valid image. 
                    Allowed extensions are only gif, jpeg, png and jpg.'
            ]);

        return $validator;
    }
}
